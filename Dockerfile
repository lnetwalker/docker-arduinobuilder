FROM debian:buster-slim

RUN \
     apt-get -y -q update \
    && apt-get -y -q update \
    && DEBIAN_FRONTEND=noninteractive apt-get -y -q --no-install-recommends install \
    make \
    curl \
    bash \
    ca-certificates

RUN \
    curl -k -o arduino-cli_0.6.0_Linux_64bit.tar.gz -LO https://github.com/arduino/arduino-cli/releases/download/0.6.0/arduino-cli_0.6.0_Linux_64bit.tar.gz \
    && tar -xzvf arduino-cli_0.6.0_Linux_64bit.tar.gz -C /usr/local/bin/

CMD ["update-ca-certificates"]
CMD ["arduino-cli", "core", "update-index"]
CMD ["/bin/bash"]

