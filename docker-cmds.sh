# commands to handle the arduino docker build container
# see https://github.com/arduino/arduino-cli

# build the docker image
docker build -t arduino-cli .

# run the container
docker run -d --privileged=true  -e container=docker --name Arduino-cli  -it arduino-cli

#get a bash in running container
docker exec -it Arduino-cli /bin/bash
